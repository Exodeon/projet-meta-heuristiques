import algo_gen
# Permet d'obtenir des informations de performances sur l'algorithme génétique :
# le score moyen des résultats obtenus et le temps moyen d'exécution

# Récupération des données en entrée standard
cities_number = int(input())
weights = []
for i in range(cities_number):
    weights.append([float(j) for j in input().split()])
minimal_tour = float(input())

# Initialisation du nombre d'itérations de l'algorithme génétique
N = 500

# Initialisation des éléments nécessaires à la comparaison des résultats obtenus
nb_it = 100
sum_score = 0
time_sum = 0

# On exécute nb_it fois la fonction algo_genetique avec les données d'entrée et on somme les scores et les temps obtenus
for i in range(nb_it):
    (path, result, time) = algo_gen.algo_genetique(weights, N)
    sum_score = sum_score + (minimal_tour/result)
    time_sum = time_sum + time

# Calcul du score moyen et du temps moyen
mean_time = time_sum/nb_it
score = sum_score/nb_it

# Affichage des résultats (score + temps moyen d'exécution)
print("Pour ", nb_it, " itérations, le score est de ", score, " et le temps moyen d'exécution est de ", mean_time, "s.")