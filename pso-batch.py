import pso

# données tsp : https://people.sc.fsu.edu/~jburkardt/datasets/tsp/tsp.html

# gestion des input pour les données
cities_number = int(input())
weights = []
for i in range(cities_number):
    weights.append([float(j) for j in input().split()])
minimal_tour = float(input())


# lancement des n exécutions avec calcul du score et du temps moyen
n = 100

sum_fitness = 0
sum_time = 0
for i in range(n):
    fitness, elapsed_time = pso.pso(weights)
    sum_time += elapsed_time
    sum_fitness += minimal_tour / fitness

print("Average time :", sum_time / n)
print("Average fitness :", sum_fitness / n)