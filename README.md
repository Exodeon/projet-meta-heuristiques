# Projet méta-heuristiques

Résolution du problème du voyageur de commerce par un algorithme exhaustif (A*) et 2 méta-heuristiques : algorithme génétique et optimisation par essaims particulaires (particle swarm optimization)

Les exemples que nous avons utilisés (dans le dossier `data`) sont tirés des sites https://people.sc.fsu.edu/~jburkardt/datasets/tsp/tsp.html et https://developers.google.com/optimization/routing/tsp.