from random import sample, random, choices
import random
import time

# Classe correspondant à un individu de la population
class Individual:
    def __init__(self, cities_list):
        # cities_list = liste des villes solution de l'individu
        self.cities_list = cities_list

    def evaluate(self, weights):
        # retourne l'évaluation de la fonction d'adaptation sur l'individu
        # entrée : weight, les distances entre chaque ville
        score = 0
        for i in range(len(self.cities_list) - 1):
            score += weights[self.cities_list[i]][self.cities_list[i + 1]]
        score += weights[self.cities_list[-1]][self.cities_list[0]]
        return score


def algo_genetique(weights, N):
    # paramètres
    population_size = 25
    cities_number = len(weights)
    # initialisation du temps pour mesurer le temps d'exécution
    init_time = time.time()

    # initialisation
    population = []
    index = [i for i in range(population_size)]
    for i in range(population_size):
        position = sample(range(cities_number), cities_number)
        population.append(Individual(position))


    # algorithme
    for i in range(N):
        new_population = []
        # Calcul des poids pour la sélection des parents
        evaluation = list(map(lambda x : x.evaluate(weights), population))
        journey_weights = list(map(lambda x : 1/x.evaluate(weights), population))

        for i in range(population_size):
            # Sélection des parents
            index_parents = random.choices(index, weights=journey_weights, k=2)
            parents = [population[index_parents[0]], population[index_parents[1]]]

            # Croisement des parents
            parent1_cities = parents[0].cities_list
            parent2_cities = parents[1].cities_list
            child_cities = []
            child_part1 = []
            child_part2 = []

            child_part1 = parent1_cities[0:cities_number // 2+1]
            child_part2 = [item for item in parent2_cities if item not in child_part1]
            child_cities = child_part1 + child_part2

            # Mutation
            if random.uniform(0, 1) < 0.5:
                position1 = int(random.random() * cities_number)
                position2 = int(random.random() * cities_number)
                temp = child_cities[position1]
                child_cities[position1] = child_cities[position2]
                child_cities[position2] = temp

            # Ajout du nouvel individu à la nouvelle population
            new_population.append(Individual(child_cities))

        # Actualisation de la population
        population = new_population

    # Calcul de la meilleure solution parmi la dernière génération de population
    evaluation = list(map(lambda x : x.evaluate(weights), population))
    position_best_result = evaluation.index(min(evaluation))
    best_result = population[position_best_result]

    # Récupération du temps final
    final_time = time.time()

    # Retourne la liste des villes de la meilleure solution, son évaluation et la durée de l'exécution de l'algorithme
    return (best_result.cities_list, best_result.evaluate(weights), final_time-init_time)
