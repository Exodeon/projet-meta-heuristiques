from random import sample, random
from time import time


class Particle:
    def __init__(self, cities_list, speed, best_solution, weights):
        self.cities_list = cities_list
        self.speed = speed
        self.best_solution = cities_list
        self.best_solution_fitness = self.evaluate(weights)

    def evaluate(self, weights):
        # évaluation de la longueur du tour
        score = 0
        for i in range(len(self.cities_list) - 1):
            score += weights[self.cities_list[i]][self.cities_list[i + 1]]
        score += weights[self.cities_list[-1]][self.cities_list[0]]
        return score


def pso(weights):
    start_time = time()
    cities_number = len(weights)
    # paramètres
    swarm_size = 25  # entre 20 et 30
    omega = 0.8  # entre 0.5 et 0.8
    # phi_p + phi_g < 3 avec phi_g > phi_p
    phi_p = 1
    phi_g = 1.5
    mutation_rate = 0.7

    max_iterations = 500

    # initialisation
    swarm = []
    global_best_solution = None
    global_best_fitness = None
    for i in range(swarm_size):
        position = sample(range(cities_number), cities_number)
        speed = [random() for n in range(cities_number)]
        new_particle = Particle(position, speed, position, weights)
        swarm.append(new_particle)
        if global_best_solution is None or new_particle.best_solution_fitness < global_best_fitness:
            global_best_solution = new_particle.cities_list.copy()
            global_best_fitness = new_particle.best_solution_fitness

    # algorithme
    iterations_count = 1
    while iterations_count < max_iterations:
        for particle in swarm:
            # calcul de la vitesse
            for d in range(cities_number):
                r_p = random()
                r_g = random()
                x_id = particle.cities_list[d]
                particle.speed[d] = omega * particle.speed[d] + phi_p * r_p * (particle.best_solution[d] - x_id) \
                                    + phi_g * r_g * (global_best_solution[d] - x_id)

            # normalisation
            particle.speed = list(map(lambda x: x/max(particle.speed), particle.speed))

            # calcul de la nouvelle position
            for d in range(cities_number):
                if random() < particle.speed[d]:
                    index_to_swap = particle.cities_list.index(global_best_solution[d])
                    particle.cities_list[index_to_swap], particle.cities_list[d] \
                        = particle.cities_list[d], particle.cities_list[index_to_swap]

            # si on a obtenu la meilleure position, ou qu'on réussit le tirage aléatoire => mutation
            if particle.cities_list == global_best_solution or random() < mutation_rate:
                index_to_swap_a, index_to_swap_b = sample(range(cities_number), 2)
                particle.cities_list[index_to_swap_a], particle.cities_list[index_to_swap_b] \
                    = particle.cities_list[index_to_swap_b], particle.cities_list[index_to_swap_a]

            # mise à jour des meilleures positions
            particle_fitness = particle.evaluate(weights)
            if particle_fitness < particle.best_solution_fitness:
                particle.best_solution = particle.cities_list
                particle.best_solution_fitness = particle_fitness
                if particle_fitness < global_best_fitness:
                    global_best_solution = particle.cities_list.copy()
                    global_best_fitness = particle_fitness

        iterations_count += 1

    return global_best_fitness, time() - start_time


if __name__ == "__main__":
    # données tsp : https://people.sc.fsu.edu/~jburkardt/datasets/tsp/tsp.html

    # gestion des input pour les données
    cities_number = int(input())
    weights = []
    for i in range(cities_number):
        weights.append([float(j) for j in input().split()])
    minimal_tour = float(input())

    fitness, elapsed_time = pso(weights)
    print("Tour of length", fitness)
    print("Elapsed time :", elapsed_time)
