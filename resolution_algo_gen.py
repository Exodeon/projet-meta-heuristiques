import algo_gen
# Permet d'obtenir un résultat pour un problème de TSP avec l'algorithme génétique

# Récupération des données en entrée standard
cities_number = int(input())
weights = []
for i in range(cities_number):
    weights.append([float(j) for j in input().split()])
minimal_tour = float(input())

# Initialisation du nombre d'itérations de l'algorithme génétique
N = 500

# Résolution à l'aide de la fonction algo_genetique
(path, result, time) = algo_gen.algo_genetique(weights, N)

# Affichage du résultat
print("chemin = ", path, ", cout = ", result)
print("Temps (s) : ", time)
