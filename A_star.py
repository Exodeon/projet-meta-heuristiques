import time

# calcule la fonction objectif pour une tournée "path" (qui peut être partielle)
def objective_value(path, weights, nb_cities):
    length = len(path)
    res = 0
    for i in range(length):
        if (i < length-1) :
            res += weights[path[i]][path[i+1]]
        elif (length == nb_cities) :    # si la tournée est complète, il faut rajouter la longueur du retour au point de départ
            res += weights[path[length-1]][path[0]]
    return res

# calcule la fonction d'évaluation pour une tournée "path" (qui peut être partielle)
def choice_value(path, weights, nb_cities):
    return (nb_cities - len(path)) + objective_value(path, weights, nb_cities)

# retourne le projet noeud à explorer dans la frontière "border"
def find_next(border, weights, nb_cities):
    choice_list = [choice_value(x,weights, nb_cities) for x in border]
    min_index = choice_list.index(min(choice_list)) # on a un problème de minimisation donc on utilise le noeud ayant la plus petite valeur
    return border[min_index]

# construit les successeurs d'un chemin partiel
def build_succ(path, nb_cities):
    cities = list(range(nb_cities))
    res = []
    for city in path :
        cities.remove(city) # cities contient toutes les villes qui ne sont pas dans path
    for city in cities :
        new_path =path.copy()
        new_path.append(city)
        res.append(new_path)
    return res

# résout le problème avec l'algorithme A*
def solve_A_star(nb_cities, weights):
    init_path=[0]
    border=[init_path] # la frontière initiale contient uniquement la racine
    while len(border)>0 :
        next_state=find_next(border, weights, nb_cities)
        if len(next_state) == nb_cities :   # si l'état choisi est une tournée complète, alors on a trouvé la solution
            return next_state
        else :
            border.remove(next_state)   # si l'état choisi n'est pas complet, on l'enlève de la frontière et on ajoute ses successeurs à la frontière
            new_nodes = build_succ(next_state, nb_cities)
            for node in new_nodes :
                border.append(node)
    return []



#------------------MAIN--------------------------------#

# tsp data : FIVE dataset of https://people.sc.fsu.edu/~jburkardt/datasets/tsp/tsp.html
# minimal tour 0 - 2 - 1 - 4 - 3, length 19
# other minimal tour 0 - 3 - 4 - 1 - 2 length 19
# weights = [[0.0, 3.0, 4.0, 2.0, 7.0],
#            [3.0, 0.0, 4.0, 6.0, 3.0],
#            [4.0, 4.0, 0.0, 5.0, 8.0],
#            [2.0, 6.0, 5.0, 0.0, 6.0],
#            [7.0, 3.0, 8.0, 6.0, 0.0]]
# cities_number = len(weights)

# récupère les données avec l'entrée standard
cities_number = int(input())
weights = []
for i in range(cities_number):
    weights.append([float(j) for j in input().split()])
minimal_tour = float(input())

start = time.time()
sol = solve_A_star(cities_number, weights)
end = time.time()
print("solution : ", sol)
print("z = ", objective_value(sol,weights,cities_number))
print("time (s) : ",end-start)
