from random import sample, random, choices
import random
import time

class Individual:
    def __init__(self, cities_list):
        self.cities_list = cities_list

    def evaluate(self, weights):
        score = 0
        for i in range(len(self.cities_list) - 1):
            score += weights[self.cities_list[i]][self.cities_list[i + 1]]
        score += weights[self.cities_list[-1]][self.cities_list[0]]
        return score


def algo_genetique(weights, N):
    # parameters
    population_size = 25


    # # tsp data : FIVE dataset of https://people.sc.fsu.edu/~jburkardt/datasets/tsp/tsp.html
    # # minimal tour 0 - 2 - 1 - 4 - 3, length 19
    # weights = [[0.0, 3.0, 4.0, 2.0, 7.0],
    #         [3.0, 0.0, 4.0, 6.0, 3.0],
    #         [4.0, 4.0, 0.0, 5.0, 8.0],
    #         [2.0, 6.0, 5.0, 0.0, 6.0],
    #         [7.0, 3.0, 8.0, 6.0, 0.0]]
    cities_number = len(weights)

    init_time = time.time()

    # initialisation
    population = []
    index = [i for i in range(population_size)]
    for i in range(population_size):
        position = sample(range(cities_number), cities_number)
        population.append(Individual(position))


    # algorithme
    for i in range(N):
        evaluation = list(map(lambda x : x.evaluate(weights), population))
        position_best_result = evaluation.index(min(evaluation))
        best_result = population[position_best_result]

        new_population = []
        journey_weights = list(map(lambda x : 1/x.evaluate(weights), population))
        for i in range(population_size):
            index_parents = random.choices(index, weights=journey_weights, k=2)
            parents = [population[index_parents[0]], population[index_parents[1]]]

            parent1_cities = parents[0].cities_list
            parent2_cities = parents[1].cities_list
            child_cities = []
            child_part1 = []
            child_part2 = []

            # bound1 = int(random.random() * cities_number)
            # bound2 = int(random.random() * cities_number)
            bound1 = int(random.random() * cities_number // 2)
            bound2 = bound1 + cities_number // 2

            lower_bound = min(bound1, bound2)
            upper_bound = max(bound1, bound2)

            child_part1 = parent1_cities[lower_bound:upper_bound+1]

            child_part2 = [item for item in parent2_cities if item not in child_part1]

            child_cities = child_part1 + child_part2

            if random.uniform(0, 1) < 0.5:
                position1 = int(random.random() * cities_number)
                position2 = int(random.random() * cities_number)
                temp = child_cities[position1]
                child_cities[position1] = child_cities[position2]
                child_cities[position2] = temp


            new_population.append(Individual(child_cities))

        population = new_population

    final_time = time.time()

    return (best_result.cities_list, best_result.evaluate(weights), final_time-init_time)
